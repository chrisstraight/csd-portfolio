<?php
 get_template_part('templates/content-blocks/headers/header-work');
 ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>


<section <?php post_class(); ?>>
  <div class="content-row">
    <div class="content-row-container">
      <div class="content-row-inner">

        <?php while (have_posts('show_posts=-1')) :
          the_post(); ?>
          <?php get_template_part('templates/content-work', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
        <?php endwhile; ?>

      </div>
    </div>
  </div>
</section>


<?php the_posts_navigation(); ?>


