<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/utils.php',                 // Utility functions
  'lib/init.php',                  // Initial theme setup and constants
  'lib/wrapper.php',               // Theme wrapper class
  'lib/conditional-tag-check.php', // ConditionalTagCheck class
  'lib/config.php',                // Configuration
  'lib/assets.php',                // Scripts and stylesheets
  'lib/titles.php',                // Page titles
  'lib/extras.php',                // Custom functions
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

// Returns parent page in URL

/* Breadcrumbs Start
http://mkoerner.de/breadcrumbs-for-wordpress-themes-with-bootstrap-3/
*/

function the_breadcrumb()
{
  if (!is_home()) {
    echo '<ol class="breadcrumb">';
    echo '<li><a href="' . get_option('home') . '">Home</a></li>';
    if (is_single()) {
      echo '<li>';
      the_category(', ');
      echo '</li>';
      if (is_single()) {
        echo '<li>';
        the_title();
        echo '</li>';
      }
    } elseif (is_category()) {
      echo '<li>';
      single_cat_title();
      echo '</li>';
    } elseif (is_page() && (!is_front_page())) {
      echo '<li>';
      the_title();
      echo '</li>';
    } elseif (is_tag()) {
      echo '<li>Tag: ';
      single_tag_title();
      echo '</li>';
    } elseif (is_day()) {
      echo '<li>Archive for ';
      the_time('F jS, Y');
      echo '</li>';
    } elseif (is_month()) {
      echo '<li>Archive for ';
      the_time('F, Y');
      echo '</li>';
    } elseif (is_year()) {
      echo '<li>Archive for ';
      the_time('Y');
      echo '</li>';
    } elseif (is_author()) {
      echo '<li>Author Archives';
      echo '</li>';
    } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {
      echo '<li>Blog Archives';
      echo '</li>';
    } elseif (is_search()) {
      echo '<li>Search Results';
      echo '</li>';
    }
    echo '</ol>';
  }
}

/* Modify ACF Color Picker */

function change_acf_color_picker()
{
  // Adds client custom colors to WYSIWYG editor and ACF color picker.
  $client_colors = array(
    "#ffffff",
    "#FACBCB",
    "#f2f2f2",
    "#F16250",
  );

  echo "<script>
  (function($){
    try {
      $.wp.wpColorPicker.prototype.options = {
        palettes: " . json_encode($client_colors) . "
      };
    }
    catch (e) {}
  })(jQuery)
  </script>";
}

add_action('acf/input/admin_head', 'change_acf_color_picker');
