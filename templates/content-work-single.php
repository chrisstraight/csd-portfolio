<?php while ( have_posts() ) :
  the_post(); ?>
  <?php
  get_template_part( 'templates/content-blocks/headers/header-work-single' );
  ?>
  <?php
  get_template_part( 'templates/content-layout' );
  ?>
  <?php
  get_template_part( 'templates/content-blocks/footer-elements/pre-footer_work' );
  ?>
<?php endwhile; ?>
