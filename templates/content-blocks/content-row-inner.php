<?php if (get_row_layout() == 'text-content'): ?>
  <?php the_sub_field('text'); ?>
<?php elseif (get_row_layout() == 'image-content'): ?>
  <img class="lazy" data-original="<?php the_sub_field('image'); ?>" src="<?php the_sub_field('image'); ?>"/>
<?php
elseif (get_row_layout() == 'text_and_image-content'): ?>
  <?php the_sub_field('text'); ?>
  <img class="lazy" data-original="<?php the_sub_field('image'); ?>" src="<?php the_sub_field('image'); ?>"/>
<?php
elseif (get_row_layout() == 'image_and_text-content'): ?>
  <img class="lazy" data-original="<?php the_sub_field('image'); ?>" src="<?php the_sub_field('image'); ?>"/>
  <?php the_sub_field('text'); ?>
<?php endif; // get_row_layout ?>
