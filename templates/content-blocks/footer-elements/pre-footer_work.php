<section id="work-footer" <?php post_class(); ?>>
  <div class="content-row">
    <div class="content-row-container">
      <div class="section-title">Additional Sample Work</div>
      <div class="content-row-inner">
        <?php
        $args = array('posts_per_page' => -1);
        $lastposts = get_posts($args);
        foreach ($lastposts as $post) :
          setup_postdata($post); ?>

            <?php get_template_part('templates/content-blocks/work-excerpt'); ?>

        <?php endforeach;
        wp_reset_postdata(); ?>

      </div>
    </div>
  </div>
  </div>
</section>
