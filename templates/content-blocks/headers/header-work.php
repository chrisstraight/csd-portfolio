<header id="work">
  <div class="content-row" <?php if (get_field('header-background-color', get_option('page_for_posts')) != ''): ?>
    style="background-color:
    <?php the_field('header-background-color', get_option('page_for_posts')); ?>"
  <?php endif; ?>>
    <div class="content-row">
      <div class="content-row-container">
        <div class="content-row-inner one-column focus-none">
          <div class="full-column">
            <?php the_field('header-copy', get_option('page_for_posts')); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
