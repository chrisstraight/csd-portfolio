<header class="sub">
  <div class="content-row" <?php if (get_field('header-background-color') != ''): ?>
    style="background-color:
    <?php the_field('header-background-color'); ?>"
  <?php endif; ?>>
    <div class="content-row-container">
      <div class="content-row-inner one-column focus-none">
        <div class="full-column">
          <?php the_field('header-copy'); ?>
        </div>
      </div>
    </div>
  </div>
</header>
