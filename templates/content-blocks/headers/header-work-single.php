<header <?php post_class("work-single"); ?>>
  <div class="content-row">
    <div class="content-row-container">
      <div class="content-row-inner one-column focus-none">
        <div class="full-column text-content">

          <p class="client"><?php the_title(); ?></p>

          <h1 class="project"><?php the_field("work-title"); ?></h1>

          <p class="project-type"><?php the_field('work-type'); ?></p>

<!--          <p class="client-info">--><?php //the_field('client-description'); ?><!--</p>-->

        </div>
      </div>
    </div>
  </div>
</header>
