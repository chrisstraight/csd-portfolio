<div class="work-thumb">
  <a href="<?php the_permalink(); ?>">
    <img class="image" src="<?php the_field('work-image'); ?>"/>
    <div class="detail-container">
      <p class="detail-title"><?php the_title(); ?></p>
      <p class="detail-subtitle"><?php the_field("work-title"); ?></p>
      <p class="detail-categories"><?php the_field('work-type'); ?></p>
    </div>
  </a>
</div>


