<?php if (have_rows('content_layout')) : ?>

<?php while (have_rows('content_layout')) :
the_row(); ?>

<div class="content-row <?php the_sub_field('layout-focus'); ?> <?php the_sub_field('section_layout'); ?>" id="<?php
// check if the repeater field has rows of data
if (have_rows('section_id')):
  // loop through the rows of data
  while (have_rows('section_id')) : the_row();
    // display a sub field value
    the_sub_field('id');
//    echo (' ');
  endwhile;
else :
  // no rows found
endif;

?>" style="background-color: <?php the_sub_field('row_background_color'); ?>;">
  <div class="content-row-container">
    <?php if (get_row_layout() == 'one-column'): ?>
    <div class="content-row-inner">
      <?php if (have_rows('column_content')): ?>
      <?php while (have_rows('column_content')) :
      the_row(); ?>

      <?php if (get_row_layout() == 'text-content'): ?>
      <div class="content-full content-text">
        <?php the_sub_field('text'); ?>
        <?php elseif (get_row_layout() == 'image-content'): ?>
        <div class="content-full content-image">
          <img src="<?php the_sub_field('image'); ?>"/>
          <?php endif; // get_row_layout ?>
        </div>
        <!-- content-full -->

        <?php endwhile; // column_content ?>
        <?php endif; // have_rows (content_column) ?>
      </div>
      <!-- content-row-inner -->
    </div>
    <!-- content-container -->
  </div>
  <!-- content-row -->

  <?php elseif (get_row_layout() == 'two-column'): ?>
    <div class="content-row-inner">
      <?php if (have_rows('left-column')): ?>
      <?php while (have_rows('left-column')):
      the_row(); ?>

      <?php if (get_row_layout() == 'text-content'): ?>
      <div class="content-left content-text">
        <h1><?php the_sub_field('text'); ?></h1>
        <?php elseif (get_row_layout() == 'image-content'): ?>
        <div class="content-left content-image">
          <img src="<?php the_sub_field('image'); ?>"/>
          <?php endif; // get_row_layout ?>
        </div>
        <!-- content-left -->

        <?php endwhile; // left-column?>
        <?php endif; // have_rows (left-column) ?>

        <?php if (have_rows('right-column')): ?>
        <?php while (have_rows('right-column')) :
        the_row(); ?>

        <?php if (get_row_layout() == 'text-content'): ?>
        <div class="content-right content-text">
          <?php the_sub_field('text'); ?>
          <?php elseif (get_row_layout() == 'image-content'): ?>
          <div class="content-right content-image">
            <img src="<?php the_sub_field('image'); ?>"/>
            <?php endif; // get_row_layout ?>
          </div>
          <!-- content-right -->

          <?php endwhile; // right-column?>
          <?php endif; // have_rows (right-column) ?>
        </div>
        <!-- content-container -->
      </div>
      <!-- content-row-inner -->
    </div> <!-- content-row -->

  <?php
  elseif (get_row_layout() == 'three-column'): ?>

    <?php if (have_rows('left-column')): ?>
      <?php while (have_rows('left-column')): the_row(); ?>

        <?php if (get_row_layout() == 'text-content'): ?>
          <div class="content-left content-text" >
          <h1><?php the_sub_field('text'); ?></h1>
        <?php elseif (get_row_layout() == 'image-content'): ?>
          <div class="content-left content-image">
          <img src="<?php the_sub_field('image'); ?>"/>
        <?php endif; // get_row_layout ?>
        </div> <!-- content-left -->

      <?php endwhile; // left-column?>
    <?php endif; // have_rows (left-column) ?>

    <?php if (have_rows('middle-column')): ?>
    <?php while (have_rows('middle-column')):
    the_row(); ?>

    <?php if (get_row_layout() == 'text-content'): ?>
    <div class="content-middle content-text">
    <h1><?php the_sub_field('text'); ?></h1>
    <?php elseif (get_row_layout() == 'image-content'): ?>
    <div class="content-middle content-image">
      <img src="<?php the_sub_field('image'); ?>"/>
      <?php endif; // get_row_layout ?>
    </div> <!-- content-left -->

    <?php endwhile; // middle-column?>
  <?php endif; // have_rows (middle-column) ?>

    <?php if (have_rows('right-column')): ?>
    <?php while (have_rows('right-column')) :
    the_row(); ?>

    <?php if (get_row_layout() == 'text-content'): ?>
    <div class="content-right content-text">
    <?php the_sub_field('text'); ?>
    <?php elseif (get_row_layout() == 'image-content'): ?>
    <div class="content-right content-image">
      <img src="<?php the_sub_field('image'); ?>"/>
      <?php endif; // get_row_layout ?>
    </div> <!-- content-right -->

    <?php endwhile; // right-column?>
  <?php endif; // have_rows (right-column) ?>

    </div> <!-- content-container -->
    </div> <!-- content-row-inner -->
    </div> <!-- content-row -->

  <?php
  endif; // one-column vs two-column ?>
  <?php endwhile; // content_layout ?>
  <?php endif; // content_layout ?>



