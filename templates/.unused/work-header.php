<header class="hero hero-work">
  <div class="hero-inner">
    <h1>
      <?php the_field("hero_headline"); ?>
    </h1>
    <p>
      <?php the_field("hero_paragraph"); ?>
    </p>
    <a href="<?php the_field("hero_button_link"); ?>" class="btn btn-primary btn-xl page-scroll">
      <?php the_field('hero_button_label'); ?>
    </a>
  </div>
</header>
