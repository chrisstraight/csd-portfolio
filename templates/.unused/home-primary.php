<section>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-5">
        <h2 class="section-heading"><?php the_field("home_primary_content_headline_a"); ?></h2>
        <hr class="light">
        <p class="text-faded"><?php the_field("home_primary_content_paragraph_a"); ?></p>
      </div>
      <div class="col-md-2">
        <img style="width: 100%; height: 100%;" src="<?php the_field('home_primary_content_divider'); ?>"/>
      </div>
      <div class="col-md-5">
        <h2 class="section-heading"><?php the_field("home_primary_content_headline_b"); ?></h2>
        <hr class="light">
        <p class="text-faded">
          <?php the_field("home_primary_content_paragraph_b"); ?>
        </p>
      </div>
    </div>
  </div>
</section>
