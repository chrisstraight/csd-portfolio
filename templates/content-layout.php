<?php if ( have_rows( 'content-row' ) ): ?>
  <?php while ( have_rows( 'content-row' ) ): the_row(); ?>
    <section <?php if ( get_sub_field( 'section_id' ) != '' ): ?>
      id="<?php the_sub_field( 'section_id' ); ?>"
    <?php endif; ?> >
      <div
        class="content-row" <?php if ( get_sub_field( 'row_background_color' )
                                       != ''
      ): ?>
        style="background-color:
        <?php the_sub_field( 'row_background_color' ); ?>"
      <?php endif; ?>
        <?php if ( get_sub_field( 'row_background_image' ) != '' ): ?>
          style="background-image:
            url('<?php the_sub_field( 'row_background_image' ); ?>')"
        <?php endif; ?>>
        <div class="content-row-container">
          <?php if ( get_sub_field( 'section-title' ) != '' ): ?>
            <div
              class="section-title"><?php the_sub_field( 'section-title' ); ?></div>
          <?php endif; ?>
          <?php if ( have_rows( 'column-layout' ) ): ?>
            <?php while ( have_rows( 'column-layout' ) ):
              the_row(); ?>
              <div class="content-row-inner <?php
                echo get_row_layout( 'column-layout' ); ?><?php
                if ( get_sub_field( 'layout-focus' ) != '' ):
                  echo( ' ' );
                  the_sub_field( 'layout-focus' );
                endif; ?>">
                <?php if ( get_row_layout() == 'one-column' ): ?>
                  <?php if ( have_rows( 'full-column' ) ): ?>
                    <?php while ( have_rows( 'full-column' ) ): the_row(); ?>
                      <div class="full-column <?php
                        echo get_row_layout( 'one-column' ); ?>">
                        <?php include 'content-blocks/content-row-inner.php' ?>
                      </div>
                    <?php endwhile; // full-column ?>
                  <?php endif; // full-column ?>
                <?php elseif ( get_row_layout() == 'two-column' ): ?>
                  <?php if ( have_rows( 'left-column' ) ): ?>
                    <?php while ( have_rows( 'left-column' ) ): the_row(); ?>
                      <div class="left-column <?php
                        echo get_row_layout( 'left-column' ); ?>">
                        <?php include 'content-blocks/content-row-inner.php' ?>
                      </div>
                    <?php endwhile; // left-column ?>
                  <?php endif; // left-column ?>
                  <?php if ( have_rows( 'right-column' ) ): ?>
                    <?php while ( have_rows( 'right-column' ) ): the_row(); ?>
                      <div class="right-column <?php
                        echo get_row_layout( 'right-column' ); ?>">
                        <?php include 'content-blocks/content-row-inner.php' ?>
                      </div>
                    <?php endwhile; // right-column ?>
                  <?php endif; // right-column ?>
                  <?php
                elseif ( get_row_layout() == 'three-column' ): ?>
                  <?php if ( have_rows( 'left-column' ) ): ?>
                    <?php while ( have_rows( 'left-column' ) ): the_row(); ?>
                      <div class="left-column <?php
                        echo get_row_layout( 'left-column' ); ?>">
                        <?php include 'content-blocks/content-row-inner.php' ?>
                      </div>
                    <?php endwhile; // left-column ?>
                  <?php endif; // left-column ?>
                  <?php if ( have_rows( 'middle-column' ) ): ?>
                    <?php while ( have_rows( 'middle-column' ) ): the_row(); ?>
                      <div class="middle-column <?php
                        echo get_row_layout( 'middle-column' ); ?>">
                        <?php include 'content-blocks/content-row-inner.php' ?>
                      </div>
                    <?php endwhile; // middle-column ?>
                  <?php endif; // middle-column ?>
                  <?php if ( have_rows( 'right-column' ) ): ?>
                    <?php while ( have_rows( 'right-column' ) ): the_row(); ?>
                      <div class="right-column <?php
                        echo get_row_layout( 'right-column' ); ?>">
                        <?php include 'content-blocks/content-row-inner.php' ?>
                      </div>
                    <?php endwhile; // right-column ?>
                  <?php endif; // right-column ?>
                  <?php
                else: ?>
                  <?php
                endif; // get_row_layout
                ?>
              </div>
              <!-- end content-row-inner -->
            <?php endwhile; // column-layout ?>
          <?php endif; // column-layout ?>
        </div>
        <!-- end content-row-container -->
      </div>
      <!-- end content-row -->
    </section>
  <?php endwhile; // content-row ?>
<?php endif; // content-row ?>
