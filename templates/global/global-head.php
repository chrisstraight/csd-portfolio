<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
  <meta name="author" content="Chris Straight">
  <meta name="description"
        content="Tulsa-based independent design consultant specializing in interactive design, user experience design, brand identity and overall solid work.">
  <?php wp_head(); ?>
  <script src="//use.typekit.net/xwa8iik.js"></script>
  <script>try {
      Typekit.load();
    } catch (e) {
    }</script>
  <link rel="icon" href="http://chrisstraight.com/favicon.ico" type="image/x-icon">
  <link rel="shortcut icon" href="http://chrisstraight.com/favicon.ico" type="image/x-icon">
  <script>
    (function (i, s, o, g, r, a, m) {
      i['GoogleAnalyticsObject'] = r;
      i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
      }, i[r].l = 1 * new Date();
      a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
      a.async = 1;
      a.src = g;
      m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-64718406-1', 'auto');
    ga('send', 'pageview');
  </script>
</head>
