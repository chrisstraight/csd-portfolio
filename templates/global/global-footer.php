<footer class="footer-global" role="contentinfo">
  <div class="content-row">
    <div class="content-row-container">
      <div class="content-row-inner two-column focus-none">
        <div class="left-column">
          <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(['theme_location' => 'primary_navigation',
              'menu_class' => 'list-inline']);
          endif;
          ?>
        </div>
        <div class="right-column">
          <p>&copy;<script>document.write(new Date().getFullYear());</script>
            Chris Straight Design. All rights reserved.
          </p>

        </div>


      </div>
    </div>
  </div>
</footer>

