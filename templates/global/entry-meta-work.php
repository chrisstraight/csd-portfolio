  <article <?php post_class(); ?>>
    <header>
      <h1 class="entry-title"><?php the_field("work-title"); ?>
      </h1>
    </header>
    <div class="entry-content">
      <?php the_field("work_description"); ?>
      <img src="<?php the_field('work-image'); ?>"/>
    </div>
    <footer>
    </footer>
  </article>
