<?php while (have_posts()) : the_post(); ?>
  <h1><?php the_field('page_title'); ?></h1>

  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content-work', 'page'); ?>
<?php endwhile; ?>
