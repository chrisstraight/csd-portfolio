<?php

use Roots\Sage\Config;
use Roots\Sage\Wrapper;

?>

<?php get_template_part('templates/global/global-head'); ?>
  <body <?php body_class(); ?>>
    <!--[if lt IE 9]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/global/global-nav');
    ?>

    <div class="content-wrapper" role="document">

        <main class="main" role="main">

          <?php include Wrapper\template_path(); ?>
        </main>
      <?php
    get_template_part('templates/content-blocks/footer-elements/pre-footer-cta');
    ?>
    </div>

    <?php
      get_template_part('templates/global/global-footer');
      wp_footer();
    ?>
  </body>
</html>
